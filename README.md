## Using this repo
This configuration uses the kitchen-linode driver for Test Kitchen. You can install it with:

```
gem install kitchen-linode
```

This driver expects your Linode API key to be found in the `LINODE_API_KEY` environment variable.

```
export LINODE_API_KEY=<your Linode API key here>
```

Then you should be able to use Test Kitchen to deploy a Linode with the bootstrap script:

```
kitchen converge
```

Once that's done, you can run the test script:

```
kitchen verify
```

...then destroy the Linode

```
kitchen destroy
```

Or you can do all of these at the same time by running:

```
kitchen test
```
